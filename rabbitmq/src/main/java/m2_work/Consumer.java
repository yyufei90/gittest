package m2_work;

import com.rabbitmq.client.*;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        //连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.126.129");
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();
        //定义队列
        c.queueDeclare("task_queue",true,false,false,null);
        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery message) throws IOException {
                String msg = new String(message.getBody());
                System.out.println("收到:"+msg);
                for (int i=0;i<msg.length();i++){
                    if (msg.charAt(i)=='.'){
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                c.basicAck(message.getEnvelope().getDeliveryTag(),false);
                System.out.println("消息处理完成\n");
            }
        };
        CancelCallback cancelCallback = new CancelCallback() {
            @Override
            public void handle(String consumerTag) throws IOException {

            }
        };
        //QOS:Quality of service
        //理解:每次抓取的消息数量
        //必须在手动ACK模式下才有效
        //如果设置成1,每次只抓取一条消息,这条消息处理完之前,不会继续抓取下一条
        c.basicQos(1);
        //消费数据
        c.basicConsume("task_queue", false, deliverCallback, cancelCallback);
        System.out.println("开始消费数据");
    }
}
