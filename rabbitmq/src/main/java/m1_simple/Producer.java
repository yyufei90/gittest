package m1_simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        //连接Rabbitmq服务器
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.126.129");
        f.setPort(5672);//5672是通信端口
        f.setUsername("admin");
        f.setPassword("admin");
        //定义队列
        Connection con=f.newConnection();
        Channel c = con.createChannel();
        c.queueDeclare("helloword",false,false,false,null);
        //发送消息
        c.basicPublish("","helloword",null,"Hello world".getBytes());
        System.out.println("消息已发送");
        c.close();
        con.close();
    }
}
