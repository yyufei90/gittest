package cn.tedu.sp091;

import cn.tedu.sp091.feign.ItemFeignClient;
import cn.tedu.sp091.feign.OrderFeignClient;
import cn.tedu.sp091.feign.UserFeignClient;
import com.tedu.sp01.pojo.Item;
import com.tedu.sp01.pojo.Order;
import com.tedu.sp01.pojo.User;
import com.tedu.web.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FeignConteoller {
    @Autowired
    private ItemFeignClient itemFeignClient;
    @Autowired
    private UserFeignClient userFeignClient;
    @Autowired
    private OrderFeignClient orderFeignClient;

    @GetMapping("/item-service/{orderId}")
    public JsonResult<List<Item>> getItems(@PathVariable String orderId){
        JsonResult<List<Item>> items = itemFeignClient.getItems(orderId);
        return items;
    }
    @GetMapping("/item-service/decreaseNumber")
    public JsonResult<?> decreaseNumber(@RequestBody List<Item> items){
        JsonResult<?> result = itemFeignClient.decreaseNumber(items);
        return result;
    }

    @GetMapping("/user-service/{userId}")
    public JsonResult<User> getUser(@PathVariable Integer userId){
        JsonResult<User> user = userFeignClient.getUser(userId);
        return user;
    }
    @GetMapping("/user-service/{userId}/score")
    public JsonResult<?> addScore(@PathVariable Integer userId,Integer score){
        JsonResult<?> result = userFeignClient.addScore(userId, score);
        return result;
    }

    @GetMapping("/order-service/{orderId}")
    public JsonResult<Order> getOrder(@PathVariable String orderId){
        JsonResult<Order> order = orderFeignClient.getOrder(orderId);
        return order;
    }
    @GetMapping("/order-service/")
    public JsonResult<?> addOrder(){
        return orderFeignClient.addOrder();
    }
}
