package cn.tedu.sp091.feign;

import com.tedu.sp01.pojo.Order;
import com.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

@Component
public class OrderFeignClientFB implements OrderFeignClient {
    @Override
    public JsonResult<Order> getOrder(String orderId) {
        return JsonResult.err("无法获取订单列表");
    }

    @Override
    public JsonResult<?> addOrder() {
        return JsonResult.err("无法增减订单");
    }
}
