package com.tedu.sp01.service;

import com.tedu.sp01.pojo.Item;
import com.tedu.sp01.pojo.Order;

import java.util.List;

public interface OrderService {
    Order getOrder(String OrderId);
    void  addOrder(Order order);
}
