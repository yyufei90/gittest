package com.tedu.sp01.service;

import com.tedu.sp01.pojo.Item;

import java.util.List;

public interface ItemService {
    List<Item> getItems(String OrderId);
    void decreaseNumbers(List<Item> list);
}
