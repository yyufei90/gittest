package cn.tedu.sp11.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.tedu.web.util.JsonResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AccessFilter extends ZuulFilter{
    /*过滤器的类型:前置,后置,路由,错误
    */
    @Override
        public String filterType() {
        return FilterConstants.PRE_TYPE;
    }
/*
过滤器添加的顺序号,返回6,
 */
    @Override
    public int filterOrder() {
        return 6;
    }
/*
针对当前请求进行判断,判断当前请求是否要执行这个过滤器的过滤代码
如果访问item-service要检查权限,如果访问其他服务则不检查权限,直接访问
 */
    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        //获取当前请求的服务id
        String serviceId = (String) ctx.get(FilterConstants.SERVICE_ID_KEY);
        if (serviceId.equals("item-service")){
            return true;//要执行权限过滤
        }
        return false;
    }
/*
过滤代码,进行权限判断
 */
    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        String token = request.getParameter("token");
        if(StringUtils.isEmpty(token)){
            //没有token继续阻止
            ctx.setSendZuulResponse(false);
            //发送提示,提示用户没有登录
            ctx.setResponseStatusCode(JsonResult.NOT_LOGIN);
            ctx.setResponseBody(JsonResult.err().code(JsonResult.NOT_LOGIN).msg("not login!").toString());
        }
        System.out.println("完成!");
        return null;//zuul过度设计，返回值在现在的版本中没有使用
    }
}
