package com.tedu.sp04.order.Feign;

import com.tedu.sp01.pojo.Item;
import com.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class ItemFeignClientFB implements ItemFeignClient{
    @Override
    public JsonResult<List<Item>> getItems(String orderId) {
        if (Math.random()<05){
            ArrayList<Item> items = new ArrayList<>();
            items.add(new Item(1,"缓存商品1",2));
            items.add(new Item(2,"缓存商品2",1));
            items.add(new Item(3,"缓存商品3",5));
            items.add(new Item(4,"缓存商品4",1));
            items.add(new Item(5,"缓存商品5",3));
            return JsonResult.ok().data(items);
        }
        return JsonResult.err("获取订单的商品列表失败");
    }

    @Override
    public JsonResult decreaseNumber(List<Item> items) {
        return JsonResult.err().msg("商品库存减少失败");
    }
}
