package com.tedu.sp04.order.service;

import com.tedu.sp01.pojo.Item;
import com.tedu.sp01.pojo.Order;
import com.tedu.sp01.pojo.User;
import com.tedu.sp01.service.OrderService;;
import com.tedu.sp04.order.Feign.ItemFeignClient;
import com.tedu.sp04.order.Feign.UserFeignClient;
import com.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {
@Autowired
private UserFeignClient userService;
@Autowired
private ItemFeignClient itemService;
    @Override
    public Order getOrder(String OrderId) {
        //TODO: 调用user-service获取用户信息
        JsonResult<User> user = userService.getUser(7);
        //TODO: 调用item-service获取商品信息
        JsonResult<List<Item>> items = itemService.getItems(OrderId);
        Order order=new Order();
        order.setId(OrderId);
        order.setUser(user.getData());
        order.setItems(items.getData());
        return order;
    }

    @Override
    public void addOrder(Order order) {
        //TODO: 调用item-service减少商品库存
         itemService.decreaseNumber(order.getItems());
        //TODO: 调用user-service增加用户积分
         userService.addScore(7, 100);
        log.info("保存订单："+order);
    }
}
