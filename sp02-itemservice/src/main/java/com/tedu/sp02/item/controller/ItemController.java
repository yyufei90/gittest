package com.tedu.sp02.item.controller;

import com.tedu.sp01.pojo.Item;
import com.tedu.sp01.service.ItemService;
import com.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@RestController
@Slf4j
public class ItemController {
    @Autowired
    private ItemService itemService;
    @Value("${server.port}")
    private int port;

    @GetMapping("/{orderId}")
    public JsonResult<List<Item>> getItems(@PathVariable String orderId) throws InterruptedException {
        log.info("server.port="+port+"orderId="+orderId);
        if (Math.random()<0.6){
            int i=new Random().nextInt(5000);
            System.out.println("延迟:"+i);
            Thread.sleep(i);
        }
        List<Item> list=itemService.getItems(orderId);
        return JsonResult.ok(list).msg("port="+port);
    }
    @PostMapping("/decreaseNumber")
    public JsonResult decreaseNumber(@RequestBody List<Item> list){
        itemService.decreaseNumbers(list);
        return JsonResult.ok();
    }
}
